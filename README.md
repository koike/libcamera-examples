# QUICK HACK: Virtme for kernel developers

Our first [blog post about virtme](https://www.collabora.com/news-and-blog/blog/2018/09/18/virtme-the-kernel-developers-best-friend/ ) showed how to use virtme for kernel development in general.

Virtme is very convenient for testing the kernel inside a virtual machine (QEMU) without the need to install and maintain a full distro other then your host machine.

In this post, we'll show you some quick hacks to use virtme in some specific usecases for your tests.

# Qemu options - avoid "Out of memory" error

Virtme doesn't select memory or number of CPUs to be used by QEMU, which sometimes causes out of memory error on boot.

Lets use a variable to pass this resources to Virtme.
Feel free to adjust the number of ram and cpu cores to be used.

```
export MEM_CPU="-m 2G -smp 2"
```

# Running Weston on virtme

## Install weston in your host machine

```
sudo apt install weston
```

## Launch virtme

This following command will launch virtme with a display, keeping the serial in the current console for convenience.

```
# NOTE: `CONFIG_DRM_VIRTIO_GPU=y` must be enabled in the kernel config.
$(host) virtme-run --graphics --rwdir /tmp --kdir $KDIR --kopt="console=ttyS0" --qemu-opts $MEM_CPU -vga virtio -serial mon:stdio
```

Where $KDIR is the path of your kernel directory.

## Launch weston

```
$(virtme) export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir
$(virtme) mkdir -p "${XDG_RUNTIME_DIR}"
$(virtme) chmod 0700 "${XDG_RUNTIME_DIR}"
$(virtme) weston --tty 1
```

[ image https://people.collabora.com/~koike/weston-virtme.png ]

# Running vimc + libcamera on virtme

To use vimc, remember to enable `CONFIG_VIDEO_VIMC=y` in kernel config.

## Install libcamera and its dependencies

```
$(host) git clone git://linuxtv.org/libcamera.git
$(host) cd libcamera

# Check README.rst for updated dependency list and install instructions
$(host) sudo apt install libgnutls28-dev openssl qtbase5-dev libqt5core5a libqt5gui5 libqt5widgets5

$(host) meson build
$(host) ninja -C build install
```

## Launch virtme + cam

To run `cam` for capturing frames by command line, we don't need a display:

```
$(host) virtme-run --rwdir /tmp --kdir $KDIR --qemu-opts $MEM_CPU
```

Option `--rwdir /tmp` allows to read and write from the `/tmp/` folder in the host machine, so we can store the captured frames.

```
$(virtme) /usr/local/bin/cam -c 1 -C --file="/tmp/libcamframe#.data" -s width=1920,height=1080
```

To view the image in your host machine:

```
$(host) ffplay  -f rawvideo -pixel_format rgb24 -video_size "1920x1080" /tmp/libcamframestream0-000005.data
```

## Launch virtme + qcam

To ran `qcam` you need to launch virtme with a display:

```
# NOTE: `CONFIG_DRM_VIRTIO_GPU=y` must be enabled in the kernel config.
$(host) virtme-run --graphics --kdir $KDIR --kopt="console=ttyS0" --qemu-opts $MEM_CPU -vga virtio -serial mon:stdio
```

Note: Nor weston or any other compositor need to be launched before qcam.

```
$(virtme) QT_QPA_PLATFORM=linuxfb /usr/local/bin/qcam -s width=1920,height=1080
```

// TODO: I had to make this change to make qcam work http://ix.io/2jqZ , since the pipeline advertises BGR24, but vimc doesn't support it.

[image https://people.collabora.com/~koike/qcam-vimc.png ]


# Running IGT graphics tests on virtme

## Install IGT tests and its dependencies

```
$(host) git clone https://gitlab.freedesktop.org/drm/igt-gpu-tools.git
$(host) cd igt-gpu-tools

# Check README.rst for updated dependency list and install instructions

$(host) sudo apt-get install \
                        piglit \
                        gcc \
                        flex \
                        bison \
                        pkg-config \
                        libatomic1 \
                        libpciaccess-dev \
                        libkmod-dev \
                        libprocps-dev \
                        libdw-dev \
                        zlib1g-dev \
                        liblzma-dev \
                        libcairo-dev \
                        libpixman-1-dev \
                        libudev-dev \
                        libxrandr-dev \
                        libxv-dev \
                        x11proto-dri2-dev \
                        meson \
                        libdrm-dev^C

$(host) meson -Drunner=enabled build
$(host) ninja -C build
```

## Running IGT on virtio-gpu

Start virtme:

```
# NOTE: `CONFIG_DRM_VIRTIO_GPU=y` must be enabled in the kernel config.
$(host) virtme-run --rwdir /tmp --graphics --kdir $KDIR --kopt="console=ttyS0" --qemu-opts $MEM_CPU -vga virtio -serial mon:stdio
```

And run the tests:

```
$(virtme) cd path/to/igt-gpu-tools

# Running specific test
$(virtme) ./build/tests/kms_flip --run-subtest wf_vblank-ts-check

# Or running a set of tests, and generating html summary
$(virtme) ./scripts/run-tests.sh -s -t "kms_*" -r /tmp/results
$(host) see /tmp/results/html/index.html
```

[image https://people.collabora.com/~koike/igt-qemu.png ]

## Running IGT on vKMS

Running tests on vKMS is essencially the same thing, with the advantage you don't need the display in virtme, since vKMS is virtual.

Disable all other displays and leave vKMS only in the kernel:

```
CONFIG_DRM_VIRTIO_GPU=n
CONFIG_DRM_VKMS=y
```

Launch virtme:

```
$(host) virtme-run --rwdir /tmp --kdir $KDIR --qemu-opts $MEM_CPU
```

And you can run tests as usuall.

# Running kmscube on virtme

## Install kmscube and its dependencies

```
$(host) git clone https://gitlab.freedesktop.org/mesa/kmscube.git
$(host) cd kmscube

# Check .gitlab-ci.yml for updated dependency list
$(host) sudo apt install \
                    build-essential \
                    pkg-config \
                    libdrm-dev \
                    libgbm-dev \
                    libegl1-mesa-dev \
                    libgles2-mesa-dev \
                    libgstreamer1.0-dev \
                    libgstreamer-plugins-base1.0-dev \
                    gstreamer1.0-plugins-base \
                    gstreamer1.0-plugins-base-apps \
                    ninja-build \
                    python3 python3-pip \
                    libpng-dev

$(host) meson build
$(host) ninja -C build
```

## Launch virtme + kmscube

```
# NOTE: `CONFIG_DRM_VIRTIO_GPU=y` must be enabled in the kernel config.
$(host) virtme-run --graphics --kdir $KDIR --kopt="console=ttyS0" --qemu-opts $MEM_CPU -vga virtio -serial mon:stdio

$(virtme) cd path/to/kmscube
$(virtme) ./build/kmscube
```

[ Image https://people.collabora.com/~koike/kmscube.png ]

# Running kms-quads on virtme

## Install kms-quads and its dependencies

```
$(host) git clone https://gitlab.freedesktop.org/daniels/kms-quads.git
$(host) cd kms-quads
$(host) meson build
$(host) ninja -C build

## Launch virtme + kms-quads

```
# NOTE: `CONFIG_DRM_VIRTIO_GPU=y` must be enabled in the kernel config.
$(host) virtme-run --graphics --kdir $KDIR --kopt="console=ttyS0" --qemu-opts $MEM_CPU -vga virtio -serial mon:stdio

$(virtme) cd path/to/kms-quads
$(virtme) TTYNO=1 ./build/kms-quads
```

[ Image https://people.collabora.com/~koike/kms-quads.png ]